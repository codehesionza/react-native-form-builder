import React from 'react';
import { ErrorBlock } from '../utility/ErrorBlock';

export class Field extends React.Component {
    getElement() {
        const { type } = this.props;

        return React.createElement(type, this.props);
    }

    render() {
        const { baseItem = 'Item', error } = this.props;
        return React.createElement(baseItem, this.props, [
            this.getElement(),
            error && React.createElement('ErrorBlock', { error })
        ]);
    }
}
