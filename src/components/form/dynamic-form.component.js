import React from 'react';
import { Field } from './Field';
import { Model } from '../../data/Model';
import { View } from 'react-native';
import { Label } from './Label';
import { SubmitButton } from './SubmitButton';

/**
 *
 */
export class DynamicFormComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            model: this.props.model || {}
        };
    }

    componentDidMount() {
        this.setModel(this.props.model);
    }

    componentDidUpdate(prevProps, prevState) {
        if (
            this.props.errors !== prevProps.errors &&
            (Array.isArray(this.props.errors) && this.props.errors.length > 0)
        ) {
            const model = prevState.model;
            model.setErrors(this.props.errors);
            this.setModel(model);
        }

        if (this.props.fields !== prevProps.fields) {
            const model = prevState.model;
            model.fields = model.getFields(this.props.fields);
            this.setModel(model);
        }

        if (this.props.data !== prevProps.data) {
            const model = prevState.model;
            model.load(this.props.data);
            this.setModel(model);
        }
    }

    setModel(model) {
        this.setState(prevState => ({
            ...prevState,
            model
        }));
    }

    onFieldChange = event => {
        const name = event.target.name;
        const value = event.target.value;
        const model = this.state.model;
        const field = model.get(name);
        model.set(name, value);
        model.validate();
        this.setModel(model);

        if (field && field.onChange) {
            field.onChange(event);
        }
    };

    onSubmit = event => {
        const { onAction } = this.props.submit;
        event.preventDefault();

        this.validateForm();

        if (!this.state.model.hasErrors()) {
            const data = this.state.model.getReturnObject();
            onAction(data);
        }
    };

    validateForm = () => {
        const model = this.state.model;
        model.validate();
        this.setModel(model);
    };

    getFieldAttribute = (name, attribute) => {
        const field = this.state.model.get(name);

        return field ? field[attribute] : undefined;
    };

    /**
     *
     * @param {mixed} value
     * @param {string} actionToUse
     */
    onDependentValueUpdate = (actionToUse, value) => {
        actionToUse(value);
    };

    render() {
        const { label = 'Submit' } = this.props.submit;

        return (
            <View>
                {this.state.model.fields.map(field => (
                    <View key={field.name}>
                        <Label>{field.label}</Label>
                        <Field
                            field={field}
                            onDependentValueUpdate={field.onDependentValueUpdate}
                            getFieldAttribute={this.getFieldAttribute}
                            onChange={value => this.onFieldChange(field.name, value)}
                        />
                    </View>
                ))}
                <SubmitButton onPress={this.onSubmit} title={label} />
            </View>
        );
    }
}
