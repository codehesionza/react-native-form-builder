import React from 'react';
import { Text } from 'react-native';

export const Label = ({ label, required = true, requiredClass = 'text-danger' }) => (
    <Text>
        {label && (
            <Text>
                {label}
                &nbsp;
                {required && <Text style={requiredClass}>*</Text>}
            </Text>
        )}
    </Text>
);
