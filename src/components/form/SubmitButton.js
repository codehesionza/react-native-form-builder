import React from 'react';
import { Text, Button, Spinner } from 'native-base';

export const SubmitButton = props => {
    const { submitting = false } = props;
    return submitting ? (
        <Button block style={styles.buttonStyle}>
            <Spinner color="white" style={styles.spinnerSize} />
        </Button>
    ) : (
        <Button block onPress={props.onPress} style={styles.buttonStyle}>
            <Text style={styles.buttonText}>{props.title}</Text>
        </Button>
    );
};

const styles = {
    spinnerSize: {
        height: 45
    },
    buttonText: {
        color: '#ffffff'
    },
    buttonStyle: {
        borderRadius: 30,
        width: 200,
        alignSelf: 'center',
        marginTop: 20
    }
};
