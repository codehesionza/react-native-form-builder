export const getEventObject = (name, value) => {
    return {
        target: {
            name,
            value
        }
    };
};
