/**
 * Adds an empty item to an array for a drop down list.
 *
 * @param {Array} items
 * @param {string} label
 * @param {any} defaultValue
 * @returns {*[]}
 */
export const addEmptyItem = (items, label = 'Select', defaultValue = undefined) => {
    return [
        {
            label,
            value: defaultValue
        },
        ...items
    ];
};
