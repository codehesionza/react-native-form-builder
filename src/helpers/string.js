/**
 *
 * @param {string} haystack
 * @param {string} needle
 * @returns {boolean}
 */
export const contains = (haystack, needle) => {
    return haystack.indexOf(needle) > -1;
};

/**
 *
 * @param {string} string
 * @returns {string}
 */
export const capitalizeFirstLetter = string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};
