import { validateFields } from '../helpers/validate';

/**
 * @typedef {Object} Validation
 */

/**
 * @typedef {Object} Field
 * @property {string} name - The unique field name
 * @property {string} label - The display label for the field
 * @property {string} placeholder - The placeholder/prompt text that is displayed
 * @property {string|ReactComponent} type - The type of field
 * @property {Array} items - The items required by a picker or list component
 * @property {Validation} validation
 */

/**
 * Class representing a Model.
 */
export class Model {
    /**
     * Creates an instance of this class
     *
     * @param props
     * @param {Field[]} fields - The list of fields that represent this model
     */
    constructor({ props = {}, fields = [] } = {}) {
        this.props = props;
        this.fields = this.getFields(this.fields);
    }

    /**
     * The function that processes and returns the required array of fields.
     * Any fields that are excluded or aren't editable are not included in the returned array.
     * @param {Field[]} fields
     * @returns {Array}
     */
    getFields(fields = []) {
        return this.cloneFields(fields).reduce((result, field) => {
            if (this.isEditable(field)) {
                field.show = true;
                const currentField = this.get(field.name);
                field.value =
                    currentField && currentField.value
                        ? currentField.value
                        : field.defaultValue || '';
                field.required =
                    field.validation &&
                    field.validation.presence &&
                    field.validation.presence.allowEmpty !== undefined
                        ? !field.validation.presence.allowEmpty
                        : false;
                result.push(field);
            }

            return result;
        }, []);
    }

    /**
     * Sets a value in a field instance (found by the name given).
     *
     * @param {string} name
     * @param {any} value
     */
    set(name, value) {
        this.fields.map(field => {
            if (field.name === name) {
                field.value = value;
            }

            return field;
        });
    }

    /**
     * Get a field object from the fields array.
     * @param name
     * @returns {Object}
     */
    get(name) {
        return this.fields.find(field => field.name === name);
    }

    /**
     * Checks whether or not this field can be edited on the form.
     *
     * @param {Object} field
     * @returns {boolean}
     */
    isEditable = field => {
        return field.editable === undefined || field.editable !== false;
    };

    /**
     * Retrieves the label from a field object, or dynamically
     * generates it based on the name if the label field does not exist.
     *
     * @param {Object} field
     * @returns {string}
     */
    getLabel = field => {
        if (field.label === undefined) {
            const header = field.name.charAt(0).toUpperCase() + field.name.slice(1);
            return header.replace(/_/g, ' ');
        }

        return field.label;
    };

    /**
     * Checks whether or not this field be shown based on
     * the item passed through to the `field.conditional(item)` function.
     *
     * @param {Object} field
     * @param {Object} item
     * @returns {boolean}
     */
    isConditional = (field, item) => {
        if (field.conditional !== undefined) {
            return field.conditional(item);
        }
        return false;
    };

    /**
     * Loads the given object of data into the fields.
     *
     * @param {Object} data
     */
    load(data = undefined) {
        if (data) {
            this.fields.map(field => {
                if (field.form && field.form.valueFn) {
                    field.value = field.form.valueFn(data);
                } else {
                    field.value = this.getValue(data, field.name);
                }

                if (field.value === undefined) {
                    field.value = field.defaultValue ? field.defaultValue : '';
                }
            });
        }
    }

    /**
     * Validates all fields in the form and sets any errors that are returned.
     */
    validate() {
        const errors = validateFields(this.fields);
        this.setErrors(errors);
    }

    /**
     * Sets the errors contained in the errors object parameter.
     *
     * @param {Object} errors
     */
    setErrors(errors = undefined) {
        if (errors) {
            this.fields.map(field => {
                if (errors[field.name]) {
                    field.error = errors[field.name][0];
                } else {
                    field.error = undefined;
                }

                return field;
            });
        } else {
            this.clearErrors();
        }
    }

    /**
     * Clears all errors in the fields
     */
    clearErrors() {
        this.fields.map(field => {
            field.error = undefined;

            return field;
        });
    }

    /**
     * Checks if this model contains any errors
     *
     * @returns {boolean}
     */
    hasErrors() {
        let errorCount = 0;
        this.fields.forEach(field => {
            if (field.error !== undefined) {
                errorCount++;
            }
        });

        return errorCount > 0;
    }

    /**
     * Takes in a flat data object and generates a multi-level object.
     * This functions turns a field like `{profile.first_name: 'Name'}` into
     * ```
     * {
     *   profile: {
     *     first_name: 'Name'
     *   }
     * }
     * ```
     *
     * @param {Object} data
     * @returns {Object}
     */
    processData = data => {
        const processed = data;
        Object.keys(data).forEach(name => {
            if (name.includes('.')) {
                let ref = processed;
                const fields = name.split('.');
                fields.forEach((field, index) => {
                    let value = undefined;
                    if (index < fields.length - 1) {
                        value = ref[field] || {};
                    } else {
                        value = data[name];
                    }
                    ref[field] = value;
                    ref = ref[field];
                });

                delete processed[name];
            }
        });

        return processed;
    };

    /**
     * Clones a fields array so references are not preserved
     * (this prevents data problems across different views/forms)
     *
     * @param {Array} fields
     * @returns {Array}
     *
     */
    cloneFields = (fields = []) => {
        return fields.map(field => Object.assign(field));
    };

    /**
     * Sets, processes and returns the final data object for submission
     * (set from the values contained within the field objects).
     *
     * @returns {Object}
     */
    getReturnObject() {
        const data = {};
        this.fields.forEach(field => {
            data[field.name] = field.value;
        });

        return this.processData(data);
    }
}
