
# react-native-form-builder

## Getting started

`$ npm install react-native-form-builder --save`

### Mostly automatic installation

`$ react-native link react-native-form-builder`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-form-builder` and add `RNFormBuilder.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNFormBuilder.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.codehesion.reactnativeformbuilder.RNFormBuilderPackage;` to the imports at the top of the file
  - Add `new RNFormBuilderPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-form-builder'
  	project(':react-native-form-builder').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-form-builder/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-form-builder')
  	```


## Usage
```javascript
import RNFormBuilder from 'react-native-form-builder';

// TODO: What to do with the module?
RNFormBuilder;
```
  